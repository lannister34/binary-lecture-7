import React, {Component} from "react";
import "./header.css";

class Header extends Component<any, any> {

    render() {
        return (
            <header key="header">
                <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                    <a className="navbar-brand" href="/">REACT CHAT</a>
                </nav>
            </header>
        )
    }
}

export default Header;