import React, {Component} from "react";
import PropTypes from "prop-types";
import {MessageProps, MessageState} from "../../interfaces/MessageInterface";
import "../../styles/message.css";

class Message extends Component<MessageProps, MessageState> {
    constructor(props: MessageProps) {
        super(props);
        console.log(this.props.id, this.props.currentUserId);
        this.state = {
            isCurrentUser: this.props.id === this.props.currentUserId
        }
    }

    static propTypes = {
        id: PropTypes.string,
        text: PropTypes.string,
        user: PropTypes.string,
        avatar: PropTypes.string,
        userId: PropTypes.string,
        editedAt: PropTypes.string,
        createdAt: PropTypes.string,
        currentUserId: PropTypes.string
    }

    render() {
        const className: string = "messages__message" + (this.state.isCurrentUser ? " messages__message-self" : "");
        const avatar: JSX.Element | string = !this.state.isCurrentUser ?
            (<div className="message__avatar-block">
                <img alt="avatar" className="message__avatar" src={this.props.avatar}/>
            </div>)
            : "";
        return (
            <div className={className} key={this.props.id}>
                {avatar}
                <div className="message__content">{this.props.text}</div>
            </div>
        )
    }
}

export default Message;