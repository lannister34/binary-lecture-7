import React, {Component} from "react";
import HeaderState from "../../../interfaces/Chat/Header/HeaderStateInterface";
import HeaderProps from "../../../interfaces/Chat/Header/HeaderPropsInterface";
import "../../../styles/header.css";

class Header extends Component<HeaderProps, HeaderState> {

    render() {
        return (
            <div key={this.props.id} className="chat__header">
                <div className="header__wrapper">
                    <div className="header__info">
                        <div className="header__name">{this.props.name}</div>
                        <div className="header__participants-count">{this.props.userCount} participants</div>
                        <div
                            className="header__messages-count">{this.props.messageCount ? this.props.messageCount + " messages" : ""}</div>
                    </div>
                    <div className="header__last-message">last message at {this.props.lastMessageTime}</div>
                </div>
            </div>
        )
    }
}

export default Header;
