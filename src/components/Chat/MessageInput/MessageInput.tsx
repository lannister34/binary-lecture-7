import React, {Component} from "react";
import PropTypes from "prop-types";
import MessageInputProps from "../../../interfaces/Chat/MessageInput/MessageInputPropsInterface";
import MessageInputState from "../../../interfaces/Chat/MessageInput/MessageInputStateInterface";
import * as MessageInputActions from "../../../store/actions/Chat/MessageInputActions";
import "../../../styles/messageInput.css";
import Message from "../../../interfaces/Chat/Message/MessageInterface";
import store from "../../../store/store";
import User from "../../../interfaces/Chat/Chat/UserInterface";
import {sendMessage} from "../../../store/actions/Chat/ChatActions";

class MessageInput extends Component<MessageInputProps, MessageInputState> {

    static propTypes = {
        currentUser: PropTypes.object
    };

    componentDidMount() {
        window.addEventListener('keydown', this.handleKeyPress);
    }

    componentWillUnmount() {
        window.removeEventListener('keydown', this.handleKeyPress);
    }

    handleKeyPress = (event: KeyboardEvent) => {
        if (event.code === "Enter") {
            const state: MessageInputState = store.getState().messageInput;
            if (state.isFocused) this.handleClick();
        }
    }

    handleChange = (event: React.FormEvent<HTMLInputElement>) => {
        const target: HTMLInputElement = event.target as HTMLInputElement;
        target.value !== store.getState().messageInput.value && (
            this.setState(() => {
                return {
                    value: target.value
                }
            })
        )
        store.dispatch(MessageInputActions.updateValue(target.value));
    }

    handleClick = () => {
        const user: User | null = this.props.currentUser;
        const state: MessageInputState = store.getState().messageInput;
        const emptyStringRegexp = new RegExp(/^\s+$/);
        if (!(user && state.value && !emptyStringRegexp.test(state.value))) {
            return;
        }
        const message: Message = {
            id: this.generateId,
            text: state.value,
            user: user.user,
            avatar: user.avatar,
            userId: user.userId,
            editedAt: "",
            createdAt: (new Date()).toISOString()
        };

        store.dispatch(MessageInputActions.updateValue(""));
        store.dispatch(sendMessage(message));
    }

    handleFocus = (event: React.FormEvent<HTMLInputElement>) => {
        !store.getState().messageInput.isFocused && (
            store.dispatch(MessageInputActions.focusOn())
        );
    }

    handleBlur = (event: React.FormEvent<HTMLInputElement>) => {
        store.getState().messageInput.isFocused && (
            store.dispatch(MessageInputActions.focusOff())
        );
    }

    get generateId() {
        return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
    }

    render() {
        const state = store.getState().messageInput;

        return (
            <div className="chat__message-input">
                <div className="message-input__input-block">
                    <input onFocus={this.handleFocus} onBlur={this.handleBlur} onChange={this.handleChange}
                           value={state.value} type="text" placeholder="Message"/>
                </div>
                <button onClick={this.handleClick} className="message-input__button"/>
            </div>
        );
    }
}

export default MessageInput;
