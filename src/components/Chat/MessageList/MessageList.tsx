import React, {Component} from "react";
import PropTypes from "prop-types";
import Message from "./Message";
import MessageListProps from "../../../interfaces/Chat/MessageList/MessageListPropsInterface";
import MessageListState from "../../../interfaces/Chat/MessageList/MessageListStateInterface";
import MessageProps from "../../../interfaces/Chat/Message/MessagePropsInterface";
import "../../../styles/messageList.css";
import store from "../../../store/store";
import * as ChatActions from "../../../store/actions/Chat/ChatActions";

class MessageList extends Component<MessageListProps, MessageListState> {
    private messageList: HTMLElement | undefined;
    private readonly messageListRef: (instance: HTMLDivElement) => void;

    constructor(props: MessageListProps) {
        super(props);
        this.messageListRef = (element: HTMLElement) => {
            this.messageList = element
        }
        this.state = {
            messageList: props.messageList.slice()
        }
    }

    static propTypes = {
        messageList: PropTypes.array,
        currentUserId: PropTypes.string,
        sendMessage: PropTypes.func
    }

    componentWillMount() {
        window.addEventListener("keydown", this.handleKeyPress);
    }

    componentWillUnmount() {
        window.removeEventListener("keydown", this.handleKeyPress);
    }

    componentDidUpdate() {
        if (this.messageList && this.props.messageList.length !== this.state.messageList.length) {
            const rect: ClientRect = this.messageList.getBoundingClientRect();
            this.messageList.scrollTo(0, rect.top + this.messageList.scrollHeight);
            this.setState(() => {
                return {
                    messageList: this.props.messageList.slice()
                }
            })
        }
    }

    handleKeyPress = (event: KeyboardEvent) => {
        if (event.code === "ArrowUp") {
            this.editMessage();
        }
    }

    editMessage = () => {
        const message: MessageProps | null = this.getLastMessage();
        if (!message) {
            return;
        }
        store.dispatch(ChatActions.showModal(message.id, this.updateMessage, this.hideModal));
    }

    updateMessage = (text: string) => {
        const message: MessageProps | null = this.getLastMessage();
        if (!message) {
            return;
        }
        message.text = text;
        store.dispatch(ChatActions.editMessage(message));
        this.hideModal();
    }

    getLastMessage() {
        if (!this.props.currentUser) {
            return null;
        }
        const reversedMessageList: Array<MessageProps> = this.props.messageList.slice().reverse();

        for (let i = 0; i < reversedMessageList.length; i++) {
            if (reversedMessageList[i].userId === this.props.currentUser.userId) {
                return reversedMessageList[i];
            }
        }
        return null;
    }

    hideModal() {
        store.dispatch(ChatActions.hideModal());
    }

    render() {

        return (
            <div ref={this.messageListRef} className="chat__message-list">
                {
                    this.props.messageList.map((message: MessageProps) => {
                        let isCurrentUser: boolean = false;
                        if (this.props.currentUser && this.props.currentUser.userId === message.userId) {
                            isCurrentUser = true;
                        }
                        return (
                            <Message key={message.id} id={message.id} avatar={message.avatar}
                                     createdAt={message.createdAt}
                                     editedAt={message.editedAt} text={message.text} user={message.user}
                                     userId={message.userId}
                                     isCurrentUser={isCurrentUser}
                            />
                        )
                    })
                }
            </div>
        );
    }
}

export default MessageList;
