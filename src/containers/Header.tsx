import React, { Component } from "react";
import {HeaderProps, HeaderState} from "../interfaces/HeaderInterface";
import "../styles/header.css";

class Header extends Component<HeaderProps, HeaderState> {
    constructor(props: HeaderProps) {
        super(props);
    }
    render() {
        return (
            <div key={this.props.id} className="chat__header">
                <div className="chat__info">
                    <div className="chat__name">{this.props.name}</div>
                    <div className="chat__participants-count">{this.props.userCount} participants</div>
                    <div className="chat__messages-count">{ this.props.messageCount ? this.props.messageCount + " messages" : ""}</div>
                </div>
                <div className="chat__last-message">last message at {this.props.lastMessageTime}</div>
            </div>
        )
    }
};

export default Header;
