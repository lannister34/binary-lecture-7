import React, {Component} from "react";
import Message from "../components/MessageList/Message";
import {MessageListProps, MessageListState} from "../interfaces/MessageListInterface";
import {MessageProps} from "../interfaces/MessageInterface";
import "../styles/messageList.css";

class MessageList extends Component<MessageListProps, MessageListState> {
    constructor(props: MessageListProps) {
        super(props);
    }

    render() {
        return (
            <div className="chat__message-list">
                {
                    this.props.messageList.map((message: MessageProps) => {
                        return (
                            <Message key={message.id} id={message.id} avatar={message.avatar} createdAt={message.createdAt}
                                     editedAt={message.editedAt} text={message.text} user={message.user}
                                     userId={message.userId} currentUserId={this.props.currentUserId}/>
                        )
                    })
                }
            </div>
        );
    }
}

export default MessageList;
