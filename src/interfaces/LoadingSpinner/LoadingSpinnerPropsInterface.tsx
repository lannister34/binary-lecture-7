export default interface LoadingSpinnerPropsInterface {
    height: string | number,
    width: string | number,
    color: string,
    radius: number
}