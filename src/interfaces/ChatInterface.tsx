interface ChatProps {
    chatId: string,
    chatName: string,
    currentUserId: string
}

interface ChatState {
    userCount: number,
    messageList: Array<any>,
    messageCount: number,
    lastMessageTime: string | null,
    isGotData: Boolean

}

export {ChatProps, ChatState}