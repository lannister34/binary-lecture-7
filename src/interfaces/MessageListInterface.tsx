import {MessageProps} from "./MessageInterface";

interface MessageListProps {
    messageList: Array<MessageProps>,
    currentUserId: string
}

interface MessageListState {

}

export {MessageListProps, MessageListState}