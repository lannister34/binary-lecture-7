interface AppProps {

}

interface AppState {
    chatId: string,
    chatName: string,
    currentUserId: string

}

export {AppProps, AppState}