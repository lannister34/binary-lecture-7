interface HeaderProps {
    id: string,
    name: string,
    userCount: number,
    messageCount: number,
    lastMessageTime: string | null
}

export default HeaderProps;