export const UPDATE_VALUE: string = "UPDATE_VALUE";
export const FOCUS_ON: string = "FOCUS_ON";
export const FOCUS_OFF: string = "FOCUS_OFF";

interface UpdateValueAction {
    type: typeof UPDATE_VALUE,
    value: string
}

interface FocusOnAction {
    type: typeof FOCUS_ON
}

interface FocusOFFAction {
    type: typeof FOCUS_OFF
}

export type MessageInputActionTypes = UpdateValueAction | FocusOnAction | FocusOFFAction;