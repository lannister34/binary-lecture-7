interface AppState {
    chatId: string,
    chatName: string
}

export default AppState;