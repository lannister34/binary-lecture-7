interface ModalStateInterface {
    value: string,
    isValueSpecified: boolean
}

export default ModalStateInterface;