import Message from "./MessageInterface";
import User from "../Chat/UserInterface";

interface MessageProps extends Message {
    isCurrentUser: boolean
}

export default MessageProps;