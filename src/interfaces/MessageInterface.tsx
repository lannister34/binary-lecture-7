interface MessageProps {
    id: string,
    text: string,
    user: string,
    avatar: string,
    userId: string,
    editedAt: string,
    createdAt: string,
    currentUserId: string
}

interface MessageState {
    isCurrentUser: boolean
}

export {MessageProps, MessageState}