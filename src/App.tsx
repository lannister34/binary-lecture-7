import React, {Component} from "react";
import Chat from "./containers/Chat";
import Header from './page/Header/Header';
import Footer from "./page/Footer/Footer";
import AppProps from "./interfaces/Chat/App/AppPropsInterface";
import AppState from "./interfaces/Chat/App/AppStateInterface";

class App extends Component<AppProps, AppState> {
    constructor(props: AppProps) {
        super(props);
        this.state = {
            chatId: "1",
            chatName: "My chat"
        }
    }

    render() {
        const header: JSX.Element = <Header />;
        const chat: JSX.Element = <Chat chatId={this.state.chatId} chatName={this.state.chatName} />
        const footer: JSX.Element = <Footer />;

        const DOMFragment: Array<JSX.Element> = [header, chat, footer];
        return (
            DOMFragment
        );
    }
}

export default App;
